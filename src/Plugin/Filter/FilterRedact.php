<?php

namespace Drupal\public_redaction\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterPluginManager;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Redact' filter.
 *
 * @Filter(
 *   id = "filter_public_redaction_redact",
 *   title = @Translation("Redact"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "example" = "foo",
 *   },
 *   weight = -10
 * )
 */
class FilterRedact extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Filter manager.
   *
   * @var \Drupal\filter\FilterPluginManager
   */
  protected $filterManager;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new FilterRedact.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer configuration array.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   * @param \Drupal\filter\FilterPluginManager|null $filter_manager
   *   Filter plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer, AccountProxyInterface $current_user, FilterPluginManager $filter_manager = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->filterManager = $filter_manager ?: \Drupal::service('plugin.manager.filter');
    $this->renderer = $renderer;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): FilterRedact {
    /** @var Drupal\filter\FilterPluginManager $filter_manager */
    $filter_manager = $container->get('plugin.manager.filter');

    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = $container->get('renderer');

    /** @var \Drupal\Core\Session\AccountProxyInterface $current_user */
    $current_user = $container->get('current_user');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $renderer,
      $current_user,
      $filter_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['keep_length'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep redacted text length'),
      '#default_value' => $this->settings['keep_length'] ?? FALSE,
      '#description' => $this->t('If selected, this setting will replace each redacted character with a placeholder character in order to preserve text length.'),
    ];

    $form['redaction_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redaction Class'),
      '#default_value' => $this->settings['redaction_class'] ?? 'redacted-text',
      '#description' => $this->t('Set the CSS class(es) used to wrap the redacted text.'),
    ];

    $form['redaction_class_visible'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redaction Class when visible'),
      '#default_value' => $this->settings['redaction_class_visible'] ?? 'redacted-text-visible',
      '#description' => $this->t('Set the CSS class(es) used to wrap the redacted text when a user has permission to see redacted text.'),
    ];

    $form['replacement'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Replacement Text'),
      '#default_value' => $this->settings['replacement'] ?? '[[ REDACTED ]]',
      '#description' => $this->t('The string to use to replace the redacted text  This is fixed length to avoid potential information leaks due to string length matching.'),
      '#states' => [
        'invisible' => [
          ':input[id="edit-filters-filter-public-redaction-redact-settings-keep-length"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (stripos($text, 'drupal-redact') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      foreach ($xpath->query('//drupal-redact') as $node) {
        if (!$this->currentUser->hasPermission('view redacted information')) {
          if ($this->settings['keep_length']) {
            $replace_text = '';
            $length = strlen($node->nodeValue);

            for ($i = 1; $i < $length; $i++) {
              $replace_text .= '█';
            }
          }
          else {
            $replace_text = $this->settings['replacement'] ?? '[[ REDACTED ]]';
          }

          $redacted = [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#attributes' => [
              'class' => $this->settings['redaction_class'] ?? 'redacted-text',
              'role' => 'img',
              'aria-label' => $this->t('Redacted text'),
            ],
            '#value' => $replace_text,
          ];
        }
        else {
          // If the user has permissions to see redacted text, replace the <drupal-redact> tag with a span and class.
          $redacted = [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#attributes' => [
              'class' => $this->settings['redaction_class_visible'] ?? 'redacted-text-visible',
            ],
            '#value' => $node->nodeValue,
          ];
        }

        $altered_html = $this->renderer->render($redacted);

        $updated_nodes = Html::load($altered_html)
          ->getElementsByTagName('body')
          ->item(0)
          ->childNodes;

        foreach ($updated_nodes as $updated_node) {
          $updated_node = $dom->importNode($updated_node, TRUE);
          $node->parentNode->insertBefore($updated_node, $node);
        }

        $node->parentNode->removeChild($node);
      }
      $result->setProcessedText(Html::serialize($dom))
        ->setCacheContexts([
          'user.permissions',
        ])
        ->addAttachments([
          'library' => [
            'public_redaction/public_redaction',
          ],
        ]);
    }

    return $result;
  }

}
