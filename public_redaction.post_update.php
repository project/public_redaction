<?php
/**
 * @file
 * Post update hooks.
 */

/**
 * Implements hook_post_update_NAME().
 */
function public_redaction_post_update_add_keep_length_setting(&$sandbox) {
  $config_factory = \Drupal::configFactory();

  $filterList = $config_factory->listAll('filter.format');

  foreach ($filterList as $filter) {
    $config = $config_factory->getEditable($filter);
    if ($config) {
      $config_filters = $config->get('filters');
      if (array_key_exists('filter_public_redaction_redact', $config_filters) && !array_key_exists('keep_length', $config_filters['filter_public_redaction_redact']['settings'])) {
        $config_filters['filter_public_redaction_redact']['settings']['keep_length'] = FALSE;
        $config->set('filters', $config_filters);
        $config->save();
      }
    }
  }

}
